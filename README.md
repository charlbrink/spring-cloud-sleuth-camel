# spring-cloud-sleuth-camel #

Extends [Spring Cloud Sleuth](https://cloud.spring.io/spring-cloud-sleuth/) for Camel routes.

## Usage ##

Add spring-cloud-sleuth-camel to the classpath of a Spring Boot application with Camel routes and you will see the span context propagated across route boundaries. 

		<dependency>
			<groupId>org.bitbucket.charlbrink</groupId>
			<artifactId>spring-cloud-sleuth-camel</artifactId>
			<version>1.0.1</version>
		</dependency>

## Contribution Guidelines ##

### Release and Deploy Steps ###
> mvn release:clean release:prepare
>
> export GPG_TTY=$(tty)
>
> mvn release:perform
>
> mvn -DstagingRepositoryId=orgbitbucketcharlbrink-nnnn nexus-staging:release