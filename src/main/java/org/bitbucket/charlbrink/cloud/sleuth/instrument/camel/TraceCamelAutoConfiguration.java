/*
 * Copyright 2013-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.charlbrink.cloud.sleuth.instrument.camel;

import org.apache.camel.Message;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.sleuth.TraceKeys;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.autoconfig.TraceAutoConfiguration;
import org.springframework.cloud.sleuth.instrument.messaging.HeaderBasedMessagingExtractor;
import org.springframework.cloud.sleuth.instrument.messaging.HeaderBasedMessagingInjector;
import org.springframework.cloud.sleuth.instrument.messaging.MessagingSpanTextMapExtractor;
import org.springframework.cloud.sleuth.instrument.messaging.MessagingSpanTextMapInjector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Charl Brink
 */
@Configuration
@ConditionalOnBean(Tracer.class)
@ConditionalOnClass({ Message.class })
@AutoConfigureAfter(TraceAutoConfiguration.class)
@ConditionalOnProperty(value = "spring.sleuth.camel.enabled", matchIfMissing = true)
@EnableConfigurationProperties({ TraceKeys.class, SleuthCamelProperties.class })
public class TraceCamelAutoConfiguration {

	@Bean
	public SleuthCamelRouteAspect sleuthCamelRouteAspect(
			TraceFromExchangeInterceptor traceFromChannelInterceptor,
			TraceToExchangeInterceptor traceToChannelInterceptor) {
		return new SleuthCamelRouteAspect(traceFromChannelInterceptor,
				traceToChannelInterceptor);
	}

	@Bean
	@ConditionalOnMissingBean
	public TraceFromExchangeInterceptor traceFromChannelInterceptor(final Tracer tracer,
			final TraceKeys traceKeys, final MessagingSpanTextMapExtractor spanExtractor,
			final MessagingSpanTextMapInjector spanInjector) {
		return new TraceFromExchangeInterceptor(tracer, traceKeys, spanExtractor,
				spanInjector);
	}

	@Bean
	@ConditionalOnMissingBean
	public TraceToExchangeInterceptor traceToChannelInterceptor(final Tracer tracer,
			final TraceKeys traceKeys, final MessagingSpanTextMapExtractor spanExtractor,
			final MessagingSpanTextMapInjector spanInjector) {
		return new TraceToExchangeInterceptor(tracer, traceKeys, spanExtractor,
				spanInjector);
	}

	@Bean
	@ConditionalOnMissingBean
	public MessagingSpanTextMapExtractor messagingSpanExtractor() {
		return new HeaderBasedMessagingExtractor();
	}

	@Bean
	@ConditionalOnMissingBean
	public MessagingSpanTextMapInjector messagingSpanInjector(final TraceKeys traceKeys) {
		return new HeaderBasedMessagingInjector(traceKeys);
	}

}
