/*
 * Copyright 2013-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.charlbrink.cloud.sleuth.instrument.camel;

import org.apache.camel.Message;
import org.springframework.cloud.sleuth.SpanTextMap;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Charl Brink
 */
public class MessagingTextMap implements SpanTextMap {
    public static final String NATIVE_HEADERS = "nativeHeaders";
	private Message delegate;

	public MessagingTextMap(final Message delegate) {
		this.delegate = delegate;
	}

	@Override
	public Iterator<Map.Entry<String, String>> iterator() {
		Map<String, String> map = new HashMap<>();
		for (Map.Entry<String, Object> entry : this.delegate.getHeaders().entrySet()) {
			if (!NATIVE_HEADERS.equals(entry.getKey()) && entry.getValue() instanceof String) {
				map.put(entry.getKey(), (String) entry.getValue());
			}
		}
		return map.entrySet().iterator();
	}

	@Override
	public void put(String key, String value) {
		if (!StringUtils.hasText(value)) {
			return;
		}
		this.delegate.setHeader(key, value);
	}
}
