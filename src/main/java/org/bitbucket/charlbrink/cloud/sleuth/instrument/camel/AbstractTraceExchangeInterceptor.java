/*
 * Copyright 2013-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.charlbrink.cloud.sleuth.instrument.camel;

import org.apache.camel.Exchange;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.SpanTextMap;
import org.springframework.cloud.sleuth.TraceKeys;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.instrument.messaging.MessagingSpanTextMapExtractor;
import org.springframework.cloud.sleuth.instrument.messaging.MessagingSpanTextMapInjector;
import org.springframework.cloud.sleuth.util.SpanNameUtil;

/**
 * @author Charl Brink
 */
abstract class AbstractTraceExchangeInterceptor {
	private static final org.apache.commons.logging.Log log = LogFactory
			.getLog(AbstractTraceExchangeInterceptor.class);

	protected static final String MESSAGE_COMPONENT = "message";

	private final Tracer tracer;
	private final TraceKeys traceKeys;
	private final MessagingSpanTextMapExtractor spanExtractor;
	private final MessagingSpanTextMapInjector spanInjector;

	@Autowired
	protected AbstractTraceExchangeInterceptor(final Tracer tracer,
			final TraceKeys traceKeys, final MessagingSpanTextMapExtractor spanExtractor,
			final MessagingSpanTextMapInjector spanInjector) {
		this.tracer = tracer;
		this.traceKeys = traceKeys;
		this.spanExtractor = spanExtractor;
		this.spanInjector = spanInjector;
	}

	protected Tracer getTracer() {
		return this.tracer;
	}

	protected TraceKeys getTraceKeys() {
		return this.traceKeys;
	}

	protected MessagingSpanTextMapInjector getSpanInjector() {
		return this.spanInjector;
	}

	protected Span buildSpan(final SpanTextMap carrier) {
		try {
			return this.spanExtractor.joinTrace(carrier);
		}
		catch (Exception e) {
			log.error("Exception occurred while trying to extract span from carrier", e);
			return null;
		}
	}

	String getExchangeName(final Exchange exchange) {
		return exchange.getContext().getName();
	}

	String getMessageExchangeName(final Exchange exchange) {
		return SpanNameUtil.shorten(MESSAGE_COMPONENT + ":" + getExchangeName(exchange));
	}

}
