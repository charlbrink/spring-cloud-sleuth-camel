/*
 * Copyright 2013-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.charlbrink.cloud.sleuth.instrument.camel;

import org.apache.camel.builder.RouteBuilder;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * @author Charl Brink
 */
@Aspect
public class SleuthCamelRouteAspect {

	private static final String ANY = "*";

	private TraceFromExchangeInterceptor traceFromExchangeInterceptor;
	private TraceToExchangeInterceptor traceToExchangeInterceptor;

	public SleuthCamelRouteAspect(
			final TraceFromExchangeInterceptor traceFromExchangeInterceptor,
			final TraceToExchangeInterceptor traceToExchangeInterceptor) {
		this.traceFromExchangeInterceptor = traceFromExchangeInterceptor;
		this.traceToExchangeInterceptor = traceToExchangeInterceptor;
	}

	@Before("execution(* org.apache.camel.builder.RouteBuilder.addRoutesToCamelContext(..))")
	public void configureCamelExchangeInterceptors(final JoinPoint joinPoint) {
		final RouteBuilder targetRouteBuilder = (RouteBuilder) joinPoint.getTarget();
		targetRouteBuilder.interceptFrom().process(this.traceFromExchangeInterceptor);
		targetRouteBuilder.interceptSendToEndpoint(ANY)
				.process(this.traceToExchangeInterceptor);
	}
}
