/*
 * Copyright 2013-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.charlbrink.cloud.sleuth.instrument.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.SpanTextMap;
import org.springframework.cloud.sleuth.TraceKeys;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.instrument.messaging.MessagingSpanTextMapExtractor;
import org.springframework.cloud.sleuth.instrument.messaging.MessagingSpanTextMapInjector;
import org.springframework.cloud.sleuth.instrument.messaging.TraceMessageHeaders;
import org.springframework.cloud.sleuth.sampler.NeverSampler;

import java.util.Map.Entry;

/**
 * @author Charl Brink
 */
public class TraceToExchangeInterceptor extends AbstractTraceExchangeInterceptor
		implements Processor {
	private static final org.apache.commons.logging.Log log = LogFactory
			.getLog(TraceToExchangeInterceptor.class);

	@Autowired
	public TraceToExchangeInterceptor(final Tracer tracer, final TraceKeys traceKeys,
			final MessagingSpanTextMapExtractor spanExtractor,
			final MessagingSpanTextMapInjector spanInjector) {
		super(tracer, traceKeys, spanExtractor, spanInjector);
	}

	@Override
	public void process(final Exchange exchange) {
		final Message inboundMessage = exchange.getIn();
		log.trace(
				String.format("TraceToExchange intercepted started Headers: %s %s %s %s",
						inboundMessage.getHeaders().toString(), getTracer(),
						getTracer().isTracing(),
						inboundMessage.getHeader(Exchange.INTERCEPTED_ENDPOINT)));
		Span parentSpan = getTracer().isTracing() ? getTracer().getCurrentSpan()
				: buildSpan(new MessagingTextMap(inboundMessage));
		SpanTextMap spanTextMap = new MessagingTextMap(inboundMessage);
		getSpanInjector().inject(parentSpan, spanTextMap);
		String name = getMessageExchangeName(exchange);
		Span currentSpan = startSpan(parentSpan, name, inboundMessage);
		for (Entry<String, String> entry : spanTextMap) {
			inboundMessage.setHeader(entry.getKey(), entry.getValue());
		}
		log.trace(String.format("TraceToExchange intercepted ending %s %s",
				getTracer().isTracing(),
				inboundMessage.getHeader(Exchange.INTERCEPTED_ENDPOINT)));
	}

	private Span startSpan(final Span span, final String name, final Message message) {
		if (span != null) {
			return getTracer().continueSpan(span);
		}
		if (Span.SPAN_NOT_SAMPLED
				.equals(message.getHeaders().get(TraceMessageHeaders.SAMPLED_NAME))) {
			return getTracer().createSpan(name, NeverSampler.INSTANCE);
		}
		return getTracer().createSpan(name);
	}

}
